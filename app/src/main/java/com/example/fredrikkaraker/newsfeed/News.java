package com.example.fredrikkaraker.newsfeed;

/**
 * Created by FredrikKaraker on 2017-03-25.
 */

public class News {
    private String title;
    private String text;
    private String link;

    public News(String title, String text, String link){
        this.title = title;
        this.text = text;
        this.link = link;

    }
    public String getTitle(){
        return title;
    }

    public String getText(){
        return text;
    }

    public String getLink() {
        return link;
    }

    @Override
    public String toString(){
        return title;
    }

}
