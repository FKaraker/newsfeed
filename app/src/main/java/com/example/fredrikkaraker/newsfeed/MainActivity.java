package com.example.fredrikkaraker.newsfeed;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.function.ToDoubleBiFunction;

public class MainActivity extends AppCompatActivity {

    private RSSHandler rss;
    private String url = "http://www.dn.se/nyheter/m/rss/";
    private ListView list;
    private final Context context = this;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_list:
                    setContentView(R.layout.activity_main);
                    return true;
                case R.id.navigation_slide:
                    //Todo: Add slide layout.
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        rss = new RSSHandler(url);
        rss.fetchXML();
        while(rss.parsingComplete);
        list = (ListView) findViewById(R.id.list);
        ArrayList<News> news = rss.getNews();
        for(News i : news){
            Log.d("news", i.getTitle());
        }
        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, news);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                News n = (News) parent.getItemAtPosition(position);
                Bundle bundle = new Bundle();
                bundle.putString("title", n.getTitle());
                bundle.putString("text", n.getText());
                bundle.putString("link", n.getLink());
                Intent intent = new Intent(context, NewsActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });


    }

}