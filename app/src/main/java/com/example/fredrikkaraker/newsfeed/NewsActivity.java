package com.example.fredrikkaraker.newsfeed;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.ButtonBarLayout;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

public class NewsActivity extends AppCompatActivity {
    private String title;
    private String text;
    private String link;
    private Context context = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        Bundle bundle = getIntent().getExtras();
        title = bundle.getString("title");
        text = bundle.getString("text");
        link = bundle.getString("link");
        TextView t =(TextView) findViewById(R.id.title);
        TextView te = (TextView) findViewById(R.id.text);
        t.setText(title);
        te.setText(text);
        Button button = (Button) findViewById(R.id.urlbutton);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putString("link", link);
                Intent intent = new Intent(context, WebActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);

            }
        });


    }
}
